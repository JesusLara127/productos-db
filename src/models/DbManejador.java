package models;
import java.sql.*;

public abstract class DbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario;
    private String database;
    private String contraseña;
    private String drive;
    private String url;

    public DbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String usuario, String database, String contraseña, String drive,String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.database = database;
        this.contraseña = contraseña;
        this.drive = drive;
        this.url = url;
        EsDrive();
    }

    public DbManejador() {
        this.drive="com.mysql.jdbc.Driver";
        this.database="sistemas";
        this.usuario="root";
        this.contraseña="";
        this.url="jdbc:mysql://localhost:3306/sistemas";
        EsDrive();
    }
    
    public boolean EsDrive(){
        boolean exito=false;
        try{
            Class.forName(this.getDrive());
            exito=true;
        }catch(ClassNotFoundException e){
            System.err.println("No se encontro driverrr"+e.getMessage());
            System.exit(-1);
        }
        return exito;
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public boolean isDriver(){
    boolean exito=false;
    
    try{
        Class.forName(drive);
        exito=true;
        
    } catch(ClassNotFoundException e){
        exito=false;
        System.err.println("Surgio un error "+ e.getMessage());
        System.exit(-1);
    }
    return exito;
    }
    
    public boolean conectar(){
    boolean exito=false;
    try{
        this.setConexion((DriverManager.getConnection(this.url,this.usuario,this.contraseña)));
        exito=true;
    }catch(SQLException e){
    exito=false;
    System.err.println("Surgio un error al conectar "+ e.getMessage());
    }
    return exito;
    }
    
    public void desconectar(){
        try{
            if(!this.conexion.isClosed()) this.getConexion().close();
        }catch(SQLException e){
            System.err.println("error no fue posible la conexion"+e.getMessage());
            
        }
    }

}
