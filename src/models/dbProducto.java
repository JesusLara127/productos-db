package models;

import java.util.ArrayList;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import views.dlgManejo;

public class dbProducto extends DbManejador implements dbPersistencia{

    dlgManejo vista;
        @Override
    public void insertar(Object objecto) throws Exception{
        Productos pro = new Productos();
        pro=(Productos) objecto;

        String consulta="insert into"
                +" productos(codigo,nombre,fecha,precio,status)values(?,?,?,?,?)";

        if(this.conectar()){
            try{
                //System.err.println("Se conecto");
                this.sqlConsulta=this.conexion.prepareStatement(consulta);
                
                this.sqlConsulta.setString(1, pro.getCodigo());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPrecio());
                this.sqlConsulta.setInt(5, pro.getStatus());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                
                System.err.println("surgio un error al insertar "+e.getMessage());
            }
        }
        else{
            System.out.println(" No fue posible conectarse ");
        }
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) objecto;

        String consulta = "update productos set nombre = ?, precio = ?,"
                + " fecha= ? where codigo = ?";

        if (this.conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //asignar valores a la consulta

                this.sqlConsulta.setString(1, pro.getNombre());
                this.sqlConsulta.setFloat(2, pro.getPrecio());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setString(4, pro.getCodigo());
                this.sqlConsulta.executeUpdate();
                this.desconectar();
                JOptionPane.showMessageDialog(vista,"Se actualizó correctamente");

            }catch (SQLException e) {
                JOptionPane.showMessageDialog(vista,"Surgio un error al actualizar" +e.getMessage());
            }
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param objecto
     * @return
     * @throws Exception
     */
        @Override
    public boolean habilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        
        pro=(Productos) objecto;
        String consulta = "update productos set  status=0 where codigo = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                this.sqlConsulta.setString(1,pro.getCodigo()); 

                this.sqlConsulta.executeUpdate();
                this.desconectar();
                System.out.println("Se Habilito");
                return true;
                
            } catch (Exception e) {
                System.out.println("Surgio un error al Habilitar "+e.getMessage());
            } 
        }
        return false;
    }

    @Override
    public boolean deshabilitar(Object objecto) throws Exception {
        Productos pro = new Productos();
        
        pro=(Productos) objecto;
        String consulta = "update productos set status =1 where codigo = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                this.sqlConsulta.setString(1,pro.getCodigo()); 

                this.sqlConsulta.executeUpdate();
                this.desconectar();
                return true;
                
            } catch (Exception e) {
                System.out.println("Surgio un error al actualizar "+e.getMessage());
            }
        }
        return false;
    }

    @Override
    public boolean isExiste(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DefaultTableModel listar() throws Exception {
        Productos pro;
        
        if(this.conectar()){
            String consulta = "select * from productos where status = 0 order by idProducto";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
            }
            
            String sql = "Select * from sistemas.productos where status = 0 order by idProducto";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }

            return Tabla;
                
        }
        return null;
       
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public DefaultTableModel listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Productos pro = new Productos();
       if(this.conectar()){
            String consulta ="Select * from productos where codigo=? and status=0";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, codigo);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setStatus(this.registros.getInt("status"));
            }
       }
       
       this.desconectar();
       return pro;
    }

    public DefaultTableModel listarD() throws Exception {
        Productos pro;
        
        if(this.conectar()){
            String consulta = "select * from productos where status = 1 order by idProducto";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //sacar los registros
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setCodigo(this.registros.getString("codigo"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("status"));
            }
            
            String sql = "Select * from sistemas.productos where status = 1 order by idProducto";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }

            return Tabla;
                
        }
        return null; 
    }
    public Object buscarD(String matricula) throws Exception {
        Productos pro = new Productos();
       if(this.conectar()){
            String consulta ="Select * from productos where codigo=? and status=1";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, matricula);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                pro.setIdProducto(this.registros.getInt("idProducto"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setPrecio(this.registros.getFloat("precio"));
                pro.setStatus(this.registros.getInt("status"));
            }
            else {
                return -1;
            
            }
       }
       this.desconectar();
       return pro;
       //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    public boolean isExiste(String matricula) throws Exception {
       Productos pro = new Productos();
       if(this.conectar()){
            String consulta ="Select * from productos where codigo=? and status=0";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, matricula);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                return false;
            }
            else {
                return true;
            
            }
       }this.desconectar();
       return true;
    }
    
}
