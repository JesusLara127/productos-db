
package models;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public interface dbPersistencia {
    public void insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto)throws Exception;
    public boolean habilitar(Object objecto)throws Exception;
    public boolean deshabilitar(Object objecto)throws Exception;
    
    public boolean isExiste(int id)throws Exception;
    public DefaultTableModel listar()throws Exception;
    public DefaultTableModel listar(String criterio)throws Exception;
    
    public Object buscar(int id)throws Exception;
    public Object buscar(String codigo)throws Exception;
}
